package pl.edu.lafk.everyday;

import java.util.stream.Collectors;

/**
 * <h2> In a nutshell </h2>
 * String got a bunch of new, handy methods
 *
 * <h2> What's in it for me? </h2>
 * <li>Python idiom of {@code "-"*40} via {@linkplain String#repeat(int)}</li>
 * <li>better and truer whitespace handling</li>
 * <li>streaming on a String using an IntStream</li>
 *
 * @author Tomasz @LAFK_pl Borek
 * @since 11
 * @see <a href="https://stackoverflow.com/questions/51266582/difference-between-string-trim-and-strip-methods-in-java-11">SO on strip vs trim</a>
 */
class NewStringAPI {
    public static void main(String[] args) {
        System.out.println("I will use new API from String class\n".repeat(10));
        System.out.println(" \t  ".isEmpty());
        stripOrTrim();
        System.out.println("=".repeat(45));
        System.out.println("  \u2000 \t ".isBlank());
        System.out.println("   a".isBlank());
        System.out.println("a   ".isBlank());
        System.out.println("   a   ".isBlank());
        System.out.println("Blank or stripped?\t".repeat(3));
        System.out.println("   a".stripLeading());
        System.out.println("a   ".stripTrailing());
        System.out.println("   a   ".strip());
        System.out.println(containsOnlyUniqChars("Dol Guldur"));
        streamOverLines("Minas Tirith\nhas triumphed\nover forces of Sauron");
    }

    private static void stripOrTrim() {
        // yes, we could trim whitespaces, but what about UTF blanks?
        String blank = "\u2000";
        System.out.println("Blank is:" + blank);
        System.out.println(blank.trim().isBlank());
        System.out.println(blank.trim().isEmpty());
        System.out.println(blank.strip().isEmpty());
        System.out.println(blank.strip().isBlank());
    }

    private static void streamOverLines(String input) {
        input.lines().forEach(System.out::println);
        input.lines()
                .filter(s -> s.contains("Minas"))
                .forEach(s -> System.out.format("Found %s", s));
    }

    private static boolean containsOnlyUniqChars(String input) {
        return input.chars().boxed().collect(Collectors.toSet()).size() == input.length();
    }
}
