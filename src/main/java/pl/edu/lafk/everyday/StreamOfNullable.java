package pl.edu.lafk.everyday;

import java.util.stream.Stream;

/**
 * <h2> In a nutshell </h2>
 * Previously, we had {@linkplain java.util.stream.Stream#of(Object)} and {@linkplain java.util.stream.Stream#of(Object[])}. One took an Object, the other vararg.
 * <b>Why was it made:</b> see {@linkplain #previously()}.
 *
 * <h2>What's in it for me?</h2>
 * <li>null check is coded in, so you don't have to code it</li>
 *
 * @see java.util.stream.Stream#ofNullable(Object)
 * @author Tomasz @LAFK_pl Borek
 * @since 9
 */
class StreamOfNullable {
    private static void previously() {
        // sometimes we build stream of things that can get us null (are nullable)
        // let's say we wish to have all OS data from properties
        // we DON'T WANT nulls however!
        // A better Java 8 version of a previous example
        Stream.of("os.name", "os.vendor", "os.arch", "os.version")
                .flatMap(propKey -> {
                    final String property = System.getProperty(propKey);
                    return property == null ? Stream.empty() : Stream.of(property);
                })
                .forEach(System.out::println);
        System.out.println("=".repeat(40));
    }

    public static void main(String[] args) {
        previously();
        Stream.of("os.name", "os.vendor", "os.arch", "os.version")
                //TODO: dive into ofNullable code to compare with one above
                .flatMap( propKey -> Stream.ofNullable(System.getProperty(propKey)))
                .forEach(System.out::println);
    }
}