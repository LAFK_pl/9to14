/**
 * Class documentation in this package follow this structure:
 *
 * <h2> In a nutshell </h2> executive summary of changes depicted in this class
 * <h2> Motivation </h2> - why was it made, if it warrants an explanation
 * <h2> What's in it for me </h2> why should you, an average dev, care
 * Optional: What to see for more.
 * Furthermore you will see which JEP it is from, since when you can use it
 *
 * Package content:
 * <li>NewStringAPI</li>
 * <li>Text Blocks</li>
 * <li>Helpful NPEs</li>
 * <li>CollectionFactories</li>
 * <li>LocalTypeInverence_Var</li>
 * <li>RecordsDemonstration</li>
 * <li>Switches</li>
 * <li>PatternMatching</li>
 * <li>Stream.ofNullable</li>
 * <li>Stream do while or until</li>
 * <li>Optional new API</li>
 * <li>Summary</li>
 *
 * @author Tomasz @LAFK_pl Borek
 * @since 9
 * @see pl.edu.lafk.JEP
 */
package pl.edu.lafk.everyday;