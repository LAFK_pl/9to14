package pl.edu.lafk.everyday;

import pl.edu.lafk.JEP;

/**
 * <h2> In a nutshell </h2>
 * Multiline Strings without convoluted 'crafting' schemes.
 *
 * <h2> What's in it for me? </h2>
 * <li>Multiline strings without + or StringBuffer.append or...</li>
 * <li>Simple JSON/XML/HTML/other structured text storing</li>
 * <li>Better readability</li>
 *
 * @see <A href="">SO strategies for NPEs</A>
 * @see <A href="https://blogs.oracle.com/javamagazine/text-blocks-come-to-java">Oracle Java Magazine article about text blocks</A>
 * @see <A href="http://www.patorjk.com/software/taag/#p=display&f=Doh&t=Tomasz%0ABorek">PatorJK AsciiArt generator</A>
 * @author Tomasz @LAFK_pl Borek
 * @since 15
 */
@JEP(url = "https://openjdk.java.net/jeps/355") // preview, J13
@JEP(url = "https://openjdk.java.net/jeps/368") // 2nd preview, J14
@JEP(url = "https://openjdk.java.net/jeps/378") // J15
class TextBlocks {

    private static void previously() {
        String multiLineString = "For some reason this String\nrequires a newline inside";
        String poem = "Seldom you have\nwhat others don't\nif you stay your hand\nthe others won't!";
        String pieceOfHTML = "<HTML>\n<HEAD>\n<title>My page<\\title>\n<\\HEAD>" +
                "<BODY>Welcome to my simple page<\\BODY><\\HTML>";

        System.out.println(multiLineString);
        System.out.println(poem);
        System.out.println(pieceOfHTML);
    }

    private static void compare() {
        String multiLineString = """   
                For some reason this String
                requires a newline inside""";
        String poem = """
                Seldom you have
                what others don't
                if you stay your hand
                the others won't!""";
        String pieceOfHTML = """
                <HTML>
                    <HEAD>
                        <title>My page<\\title>
                    <\\HEAD>
                    <BODY>
                        Welcome to my simple page
                    <\\BODY>
                <\\HTML>""";

        System.out.println(multiLineString);
        System.out.println(poem);
        System.out.println(pieceOfHTML);

    }

    private static void bad() {
        // mind the spaces in the next line
        String noOpeningLine = """   
            For some reason this String
        requires a newline inside""";
        String noLineTerminator1 = """
                """;
        String noLineTerminator2 = """
                """;
        System.out.println(noOpeningLine);
    }

    private static void lines() {
        String entry = """
                Minas Tirith
                has triumphed
                over forces of Sauron""";

        entry.lines()
                .filter(l -> l.contains("Sauron"))
                .forEach(s -> System.out.format("Found line! %s%n", s));
    }

    private static void whoami() {
        System.out.println("""
                                                                                                                               \s
                TTTTTTTTTTTTTTTTTTTTTTT                                                                                        \s
                T:::::::::::::::::::::T                                                                                        \s
                T:::::::::::::::::::::T                                                                                        \s
                T:::::TT:::::::TT:::::T                                                                                        \s
                TTTTTT  T:::::T  TTTTTTooooooooooo      mmmmmmm    mmmmmmm     aaaaaaaaaaaaa      ssssssssss   zzzzzzzzzzzzzzzzz
                        T:::::T      oo:::::::::::oo  mm:::::::m  m:::::::mm   a::::::::::::a   ss::::::::::s  z:::::::::::::::z
                        T:::::T     o:::::::::::::::om::::::::::mm::::::::::m  aaaaaaaaa:::::ass:::::::::::::s z::::::::::::::z\s
                        T:::::T     o:::::ooooo:::::om::::::::::::::::::::::m           a::::as::::::ssss:::::szzzzzzzz::::::z \s
                        T:::::T     o::::o     o::::om:::::mmm::::::mmm:::::m    aaaaaaa:::::a s:::::s  ssssss       z::::::z  \s
                        T:::::T     o::::o     o::::om::::m   m::::m   m::::m  aa::::::::::::a   s::::::s           z::::::z   \s
                        T:::::T     o::::o     o::::om::::m   m::::m   m::::m a::::aaaa::::::a      s::::::s       z::::::z    \s
                        T:::::T     o::::o     o::::om::::m   m::::m   m::::ma::::a    a:::::assssss   s:::::s    z::::::z     \s
                      TT:::::::TT   o:::::ooooo:::::om::::m   m::::m   m::::ma::::a    a:::::as:::::ssss::::::s  z::::::zzzzzzzz
                      T:::::::::T   o:::::::::::::::om::::m   m::::m   m::::ma:::::aaaa::::::as::::::::::::::s  z::::::::::::::z
                      T:::::::::T    oo:::::::::::oo m::::m   m::::m   m::::m a::::::::::aa:::as:::::::::::ss  z:::::::::::::::z
                      TTTTTTTTTTT      ooooooooooo   mmmmmm   mmmmmm   mmmmmm  aaaaaaaaaa  aaaa sssssssssss    zzzzzzzzzzzzzzzzz
                                                                                                                               \s
                                                                                                                               \s
                                                                                                                               \s
                                                                                                                               \s
                                                                                                                               \s
                                                                                                                               \s
                                                                                                                               \s
                                                                                                                               \s
                                                                                                                               \s
                BBBBBBBBBBBBBBBBB                                                           kkkkkkkk                           \s
                B::::::::::::::::B                                                          k::::::k                           \s
                B::::::BBBBBB:::::B                                                         k::::::k                           \s
                BB:::::B     B:::::B                                                        k::::::k                           \s
                  B::::B     B:::::B   ooooooooooo   rrrrr   rrrrrrrrr       eeeeeeeeeeee    k:::::k    kkkkkkk                \s
                  B::::B     B:::::B oo:::::::::::oo r::::rrr:::::::::r    ee::::::::::::ee  k:::::k   k:::::k                 \s
                  B::::BBBBBB:::::B o:::::::::::::::or:::::::::::::::::r  e::::::eeeee:::::eek:::::k  k:::::k                  \s
                  B:::::::::::::BB  o:::::ooooo:::::orr::::::rrrrr::::::re::::::e     e:::::ek:::::k k:::::k                   \s
                  B::::BBBBBB:::::B o::::o     o::::o r:::::r     r:::::re:::::::eeeee::::::ek::::::k:::::k                    \s
                  B::::B     B:::::Bo::::o     o::::o r:::::r     rrrrrrre:::::::::::::::::e k:::::::::::k                     \s
                  B::::B     B:::::Bo::::o     o::::o r:::::r            e::::::eeeeeeeeeee  k:::::::::::k                     \s
                  B::::B     B:::::Bo::::o     o::::o r:::::r            e:::::::e           k::::::k:::::k                    \s
                BB:::::BBBBBB::::::Bo:::::ooooo:::::o r:::::r            e::::::::e         k::::::k k:::::k                   \s
                B:::::::::::::::::B o:::::::::::::::o r:::::r             e::::::::eeeeeeee k::::::k  k:::::k                  \s
                B::::::::::::::::B   oo:::::::::::oo  r:::::r              ee:::::::::::::e k::::::k   k:::::k                 \s
                BBBBBBBBBBBBBBBBB      ooooooooooo    rrrrrrr                eeeeeeeeeeeeee kkkkkkkk    kkkkkkk\s
                """);

        System.out.println("""
                 .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.\s
                | .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |
                | |   _____      | || |      __      | || |  _________   | || |  ___  ____   | || |              | || |   ______     | || |   _____      | |
                | |  |_   _|     | || |     /  \\     | || | |_   ___  |  | || | |_  ||_  _|  | || |              | || |  |_   __ \\   | || |  |_   _|     | |
                | |    | |       | || |    / /\\ \\    | || |   | |_  \\_|  | || |   | |_/ /    | || |              | || |    | |__) |  | || |    | |       | |
                | |    | |   _   | || |   / ____ \\   | || |   |  _|      | || |   |  __'.    | || |              | || |    |  ___/   | || |    | |   _   | |
                | |   _| |__/ |  | || | _/ /    \\ \\_ | || |  _| |_       | || |  _| |  \\ \\_  | || |              | || |   _| |_      | || |   _| |__/ |  | |
                | |  |________|  | || ||____|  |____|| || | |_____|      | || | |____||____| | || |   _______    | || |  |_____|     | || |  |________|  | |
                | |              | || |              | || |              | || |              | || |  |_______|   | || |              | || |              | |
                | '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |
                 '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'\s""");
    }

    private static void intro() {
        whoami();
        System.out.println("""
                 ____  ____   __    __  __                    \s
                ( ___)(  _ \\ /__\\  (  \\/  )                   \s
                 )__)  )___//(__)\\  )    (                    \s
                (____)(__) (__)(__)(_/\\/\\_)                   \s

                       __o__                                                                               \s
                         |                                                                                 \s
                        / \\                                                                                \s
                        \\o/      o__ __o/   o      o     o__ __o/                                          \s
                         |      /v     |   <|>    <|>   /v     |                                           \s
                        < >    />     / \\  < >    < >  />     / \\                                          \s
                \\        |     \\      \\o/   \\o    o/   \\      \\o/                                          \s
                 o       o      o      |     v\\  /v     o      |                                           \s
                 <\\__ __/>      <\\__  / \\     <\\/>      <\\__  / \\ \s

                   __    ___    __    ____  ____  __  __  _  _\s
                  /__\\  / __)  /__\\  (  _ \\( ___)(  \\/  )( \\/ )
                 /(__)\\( (__  /(__)\\  )(_) ))__)  )    (  \\  /\s
                (__)(__)\\___)(__)(__)(____/(____)(_/\\/\\_) (__)                """);
    }

    private static void jsonWithStringInterpolationAlmost(String name, int age) {
        System.out.format("""
            { 
                "name" : %s,
                "age" : %d
            }%n""", name, age);

        System.out.println("""
            { 
                "name" : $1,
                "age" : $2
            }""".replace("$1", name).replace("$2", String.valueOf(age)));
    }

    public static void main(String[] args) {
        intro();
        System.out.println("*".repeat(40));
        previously();
        System.out.println("*".repeat(40));
        compare();
        System.out.println("*".repeat(40));
        bad();
        System.out.println("*".repeat(40));
        lines();
        jsonWithStringInterpolationAlmost("Rufus", 33);
    }
}
