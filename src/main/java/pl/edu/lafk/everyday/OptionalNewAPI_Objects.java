package pl.edu.lafk.everyday;

import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static java.util.Objects.requireNonNullElse;

/**
 * <h2> In a nutshell </h2>
 * {@linkplain java.util.Optional} got new API, which in turn caused other places to be able to use it.
 * While Optional is good for library-makers mostly, the changed API are good for everybody.
 *
 * <h2> Motivation behind it: </h2>
 * It was about time as previous API wasn't addressing major pain-points. People overused it (see Brian's answer).
 * Once that was done the change here was natural to follow as it was also about null-handling.
 *
 * <h2> What's in it for me? </h2>
 * null-handling idioms
 *
 * @apiNote in a similar vein, take a look at methods that check index to deal with IndexOutOfBounds family of exceptions!
 *
 * @author Tomasz @LAFK_pl Borek
 * @see <A href="https://stackoverflow.com/a/26328555/999165">Brian Goetz answer about Optional</A>
 * @see Objects#requireNonNullElse(Object, Object)
 * @see Objects#requireNonNullElseGet(Object, Supplier)
 * @since 9
 */
class OptionalNewAPI_Objects {
    private static void previously() {
        // sometimes we build stream of things that can get us null (are nullable)
        // let's say we wish to have all OS data from properties
        System.out.println(Stream.of("os.name", "os.vendor", "os.arch", "os.version")
                .map(System::getProperty)
                .reduce((s, s2) -> s + " : " + s2));
        System.out.println("=".repeat(40));
        // we could replace the null with a default value, ternary op could do well here
        System.out.println(Stream.of("os.name", "os.vendor", "os.arch", "os.version")
                .flatMap(propKey -> {
                    final String property = System.getProperty(propKey);
                    return property == null ? Stream.empty() : Stream.of(property);
                })
                .reduce((s, s2) -> s + " : " + s2));
    }

    public static void main(String[] args) {
        previously();
        System.out.println("=".repeat(40));
        // TODO: Notice the Optional-borrowed syntax
        System.out.println(Stream.of("os.name", "os.vendor", "os.arch", "os.version")
                .map(propKey -> requireNonNullElse(System.getProperty(propKey),
                        String.format("no %s information", propKey)))
                .reduce((s, s2) -> s + " : " + s2));
        System.exit(0);
    }
}
