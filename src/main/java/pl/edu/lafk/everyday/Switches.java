package pl.edu.lafk.everyday;

import pl.edu.lafk.JEP;

import java.util.List;

/**
 * <h2> In a nutshell </h2>
 * Less verbose form of a known flow-control mechanism with lambda arrows used in labels and {@code yield} keyword for returning a value which may be used in {@code return} keyword.
 *
 * <h2> Motivation </h2>
 * <li>prep work for Pattern Matching, JEP 305</li>
 * <li>fix for fall-through requiring code when unnecessary (plus it's confusing)</li>
 * <li>scoping in {@code switch} block - entire thing is one block, not just cases</li>
 * <li>making {@code switch} an expression for previously you couldn't use it as a return value</li>
 * <h2> What's in it for me </h2>
 * <li>{@code switch} can be {@code return}ed now, as an expression</li>
 * <li>new {@code case} labels (arrows) are terser and easier to get</li>
 * <li><old {@code switch}es can be left as they are, as statements/li>
 * <li>if your {@code case} happens to be multi-line, it can still {@code yield} result</li>
 * <li>enum switches with new form have compiler checks to make sure you didn't forget a label</li>
 *
 * @see "Any version with a preview for earlier proposals"
 * @see <A href="https://blogs.oracle.com/javamagazine/inside-java-13s-switch-expressions-and-reimplemented-socket-api">relevant blog post by Raoul Gabriel Urma and Richard Warburton</A>
 * @author Tomasz @LAFK_pl Borek
 * @since 14
 */
@JEP(url = "https://openjdk.java.net/jeps/325") // 1st preview where break was overloaded, was confusing
@JEP(url = "https://openjdk.java.net/jeps/354") // 2nd preview, proposed yield instead
@JEP(url = "https://openjdk.java.net/jeps/361")
class Switches {

    static boolean previouslyNoDefaultIsItWeekend(int day) {
        boolean result = false;
        switch (day) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5: break;
            case 6:
            case 7:
                result = true; break;
        }
        return result;
    }
    static boolean previouslyWithDefaultIsItWeekend(int day) {
        boolean result = false;
        switch (day) {
            default: break;
            case 6:
            case 7:
                result = true; break;
        }
        return result;
    }
    static boolean nowIsItWeekend(int day) {
        return switch (day) {
            default -> false;
            case 6, 7 -> true;
        };
    }
    static boolean yieldsIsItWeekend(int day) {
        return switch (day) {
            default: yield false;
            case 6, 7: yield true;
        };
    }

    public static void main(String[] args) {
        final String sep = "=".repeat(15);
        List.of(5,7,13).forEach( i ->
        {
            System.out.format("%s for i = %d, order: no default, default, new -> labels, yield %s%n", sep, i, sep);
            System.out.println(previouslyNoDefaultIsItWeekend(i));
            System.out.println(previouslyWithDefaultIsItWeekend(i));
            System.out.println(nowIsItWeekend(i));
            System.out.println(yieldsIsItWeekend(i));
        });
    }
}