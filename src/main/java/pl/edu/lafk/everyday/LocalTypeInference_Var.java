package pl.edu.lafk.everyday;

import pl.edu.lafk.JEP;

import java.util.*;
import java.util.stream.Stream;

import static java.lang.System.out;

/**
 * <h2> in a nutshell </h2>
 * less typing since left side of assignment is figured out (inferred) by the compiler based on the right side.
 *
 * <h2> Motivation </h2>
 * <li>less boilerplate</li>
 * <li>all statically typed langs have it: c++ auto, c# var, scala var/val, tested, not controversial feature</li>
 * <li>lambdas paved the way</li>
 *
 * <h2> What's in it for me? </h2>
 * <li>`var` keyword and less typing whenever you declare things</li>
 * <li>except for class fields - it's <b>LOCAL VARIABLE</b> type inference</li>
 * <li>var in lambda is useful only to annotate lambda params for marking purposes, like Intellij's {@code @NotNull} annotation</li>
 *
 * @author Tomasz @LAFK_pl Borek
 * @since 10
 * @since 11 for lambda parameters
 */
@JEP(url = "https://openjdk.java.net/jeps/286") // J10
@JEP(url = "https://openjdk.java.net/jeps/323") // var in lambda
class LocalTypeInference_Var {
    List<String> ss = new ArrayList<>();
//    var stringList = new ArrayList<String>(); // TODO: see, doesn't work here

    private static void previously() {
        List<Map<Integer, Set<String>>> nestedThingy = new ArrayList<>();
        final Stream<Map<Integer, Set<String>>>stream = nestedThingy.stream();
    }

    public static void main(String[] args) {
        previously();
        var now = new ArrayList<HashMap<Integer, Set<String>>>();
        out.printf("now is instance of List %s%n", now instanceof List);
        out.printf("now is instance of ArrayList %s%n", now instanceof ArrayList);
        out.printf("now is assignable from an ArrayList %s%n", now.getClass().isAssignableFrom(ArrayList.class));
        out.printf("now is assignable from a List %s%n", now.getClass().isAssignableFrom(List.class));
        var stream = now.stream();
    }
}