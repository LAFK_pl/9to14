package pl.edu.lafk.everyday;

import pl.edu.lafk.JEP;

import java.util.*;

import static java.lang.System.*;

/**
 * <h2> In a nutshell </h2>
 * Maps, Sets and Lists have now factories to create their IMMUTABLE versions
 *
 * <b>Why was it made:</b> see {@linkplain #previously()}.
 *
 * <h2>Questions and doubt points</h2>
 * <li>why so many methods and not only varargs?</li>
 * <li>why copyOf?</li>
 * <li>why {@code Map.ofEntries}? (type-safety, boxed)</li>
 * <li>why on interface and not on a class / instances, what's wrong with {@code HashMap.of}?</li>
 * <li>why immutable and not mutable? copying ctor?</li>
 *
 * <h2>Motivation behind it:</h2>
 * <li><b>simplicity</b> - simple version isn't cool! Cool one isn't simple in doing and is nightmarish in maintenance</li>
 * <li>libs already have it so why the rush? (Guava)</li>
 * <li>how to make it play well with value types?</li>
 * <li>backward compatibility</li>
 *
 * <h2>What's in it for me?</h2>
 * <li>quick immutable collection idiom</li>
 * <li>up to 10 elements - fast and GC-friendly, then vararg</li>
 * <li>quick mutable collection idiom: through copy c-tor</li>
 *
 * @author Tomasz @LAFK_pl Borek
 * @since 9
 */
@JEP(url = "https://openjdk.java.net/jeps/269")
class CollectionFactories {
    Set<String> set = new HashSet<>();

    private static void previously() {
        Set<String> set = new HashSet<>();
        set.add("Reason #1: verbose creation of an IMMUTABLE set");
        set.add("forget double braces cause perf, class loading and potential ref escape");
        set.add("stream.of(...).collect is nice but not for maps");
        set = Collections.unmodifiableSet(set);
    }

    public static void main(String[] args) {
        previously();
        //TODO: how many of methods are needed to change the lightbulb?
//        List.of();
        Set.of();
        Map.of();

        Integer thisWillChangeLater = 9;
        Character test = 'd';
        final Set<Character> chars = Set.of('a', 'b', 'c', test);
//        out.println(chars); // note the order
//        chars.remove('a'); // TODO: uncomment the line and see that it throws
        test = 'e';
        out.println(chars);
        final List<Integer> immutableInts = List.of(1, 2, 3, 4, 5, 6, thisWillChangeLater);
        thisWillChangeLater += 1;
        out.println(immutableInts);
        final ArrayList<Integer> mutableInts = new ArrayList<>(immutableInts);
        out.println(mutableInts);
        mutableInts.add(thisWillChangeLater); // no throwing - mutableInts is mutable
        thisWillChangeLater += 1;
        mutableInts.remove(0); // no throwing - mutableInts is mutable
        out.println(mutableInts);
    }
}