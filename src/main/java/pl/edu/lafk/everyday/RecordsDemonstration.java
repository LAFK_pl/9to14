package pl.edu.lafk.everyday;


import pl.edu.lafk.JEP;

import java.net.URL;
import java.util.Objects;

/**
 * <h2> In a nutshell </h2>
 * One-liner for data classes declaration instead of coding or Lombok-generating.
 *
 * <h2>Questions and doubt points</h2>
 * <li>anemic domains FTW, WTF</li>
 * <li>OO not much?</li>
 * <li>how it really works</li>
 * <li>how to satisfy tuples, value objects, anemic domains and algebra types</li>
 *
 * <h2>Motivation behind it:</h2>
 * <li><b>intent-carrying data carriers</b> - data-classes with get/set, hashCode, equals, toString AND c-tors just carry data</li>
 * <li>modelling data as data, then boilerplate will decrease as well</li>
 * <li><b>names matter</b> - thus Records over tuples</li>
 * <li>yields itself well to pattern matching, sealed classes, new and safe serialization</li>
 *
 * <h2> What's in it for me? </h2>
 * <li>one-liner for a final data class with c-tor, getters, equals, hashCode and toString!</li>
 * <li>without set methods cause immutable!</li>
 * <li>a customizable idiom for all those anemic domains! Add your fields (private final or static), methods, override those generated...</li>
 *
 * @see <a href="https://blogs.oracle.com/javamagazine/records-come-to-java">Java Mag: Records come to Java, by Ben Evans</a>
 * @see <a href="https://vladmihalcea.com/java-records-guide/">Vlad Mihalcea's blog post</a>
 *
 * @since 14
 */
@JEP(url = "https://openjdk.java.net/jeps/359") // 14th Preview
@JEP(url = "https://openjdk.java.net/jeps/384") // 15th 2nd Preview
class RecordsDemonstration {

    record Violinist(String name, String surname, int age) {}

    public static void main(String... args) {
        var myself = new Violinist("Tomasz", "Borek", 33);
        System.out.println(new Violinist("Henryk", "Wieniawski", 33));
        System.out.println(myself.name());
        System.out.println(new Violinist("Niccolo", "Paganini", 33));
        final Violinist hilary = new Violinist("Hilary", "Hahn", 33);
        System.out.println(hilary);
        System.out.println(myself.equals(hilary));

        System.out.println(new NewPoint(5,9));
    }
}
// Previously
class Point {
    private final int x;
    private final int y;

    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    int x() { return x; }
    int y() { return y; }

    public boolean equals(Object o) {
        if (!(o instanceof Point)) return false;
        Point other = (Point) o;
        return other.x == x && other.y == y;
    }

    public int hashCode() {
        return Objects.hash(x, y);
    }

    public String toString() {
        return String.format("Point[x=%d, y=%d]", x, y);
    }
}

// Now
record NewPoint(int x, int y) {
//TODO
//    private final String label = "set";

    @Override
    public String toString() {
        return "NewPoint{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}