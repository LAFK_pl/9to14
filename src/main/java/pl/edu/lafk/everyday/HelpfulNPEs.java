package pl.edu.lafk.everyday;

import pl.edu.lafk.JEP;

/**
 * <h2> In a nutshell </h2>
 * Analyzing bytecode lines gives quite precise information about NPE reasons - including what exactly happened - instead of just offering a line where it happened.
 * This helps in tricky scenarios like those in {@linkplain #previously()} method.
 *
 * <h2> Motivation </h2>
 * NPE used to only offer a line, which meant you needed to check where exactly did the null happen. Which could require tracing few different object graphs to find the offender.
 * Since source line often becomes several opcodes and thus bytecode has more information why not offer it along with precise reason for NPE, readable for humans?
 *
 * <h2> What's in it for me </h2>
 *  <li>precise tell where the problem lies</li>
 *  <li>faster NPE elimination</li>
 *
 * @apiNote remember about the flag {@code -XX:+ShowCodeDetailsInExceptionMessages}. In future releases it's supposed to be on, for now it's off by default.
 * @author Tomasz @LAFK_pl Borek
 * @see <A href="https://stackoverflow.com/questions/410890/how-to-trace-a-nullpointerexception-in-a-chain-of-getters?rq=1">SO strategies for NPEs</A>
 * @see <A href="https://github.com/dustinmarx/javademos/blob/master/src/dustin/examples/npe/NpeDemo.java">NPE extensive demoes by Dustin Marxs</A>
 * @see <A href="https://marxsoftware.blogspot.com/2019/10/better-npe-messages-in-jdk-14.html">Dustin's blog post that could've kicked off the whole thing</A>
 * @since 14
 */
@JEP(url = "https://openjdk.java.net/jeps/358")
class HelpfulNPEs {

    private static void previously() {
        String textBlockExplainingTheProblem = """
                Null is not tricky when it's easy to see what in the line causes it, like here.
                Null becomes \ntricky\n in cases like:
                    arr[i][j][k]
                    a.b.c.d
                    a.i = b.j
                    x().y().i
         """;
        System.out.println(textBlockExplainingTheProblem);
    }

    public static void main(String[] args) {
        previously();
        class B { int i;}
        class A { B b; A(B b) { this.b = b;}}
        B b = new B();
        A a = new A(b);
        A a2 = new A(null);
        a.b.i = 99;
        a2.b.i = 99;
    }
}
