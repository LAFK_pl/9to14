package pl.edu.lafk.everyday;

import pl.edu.lafk.JEP;

/**
 * <h2> In a nutshell </h2>
 * Better conditional extraction.
 *
 * <h2> Motivation </h2>
 * <li>follows new switches syntax</li>
 * <li>less casting where no need for casting</li>
 * <li>other languages long have it</li>
 *
 * <h2> What's in it for me </h2>
 * <li>{@code switch} can be {@code return}ed now, as an expression</li>
 * <li>new {@code case} labels (arrows) are terser and easier to get</li>
 * <li>enum switches with new form have compiler checks to make sure you didn't forget a label</li>
 *
 * @see <A href="https://blogs.oracle.com/javamagazine/inside-java-13s-switch-expressions-and-reimplemented-socket-api">relevant blog post by Raoul Gabriel Urma and Richard Warburton</A>
 * @see <A href="https://cr.openjdk.java.net/~briangoetz/amber/pattern-match.html">Brian Goetz's writing on it</A>
 * @author Tomasz @LAFK_pl Borek
 * @since 14 as preview
 */
@JEP(url = "305") // J14, Preview
@JEP(url = "375") //J15, 2nd Preview
class PatternMatching {

}
