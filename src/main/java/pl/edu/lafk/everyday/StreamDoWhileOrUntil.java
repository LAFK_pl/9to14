package pl.edu.lafk.everyday;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * <h2> In a nutshell </h2>
 *
 *
 * <h2> Motivation behind it: </h2>
 * Filtering is nice. But FP knows takeWhile and dropWhile for... well, a while now. ;P
 *
 * <h2> Question and doubt points </h2>
 * <li>{@linkplain java.util.stream.Stream#takeWhile} takes until <i>!condition</i> and only while <i>condition</i>.</li>
 * <li>{@linkplain java.util.stream.Stream#dropWhile} drops until <i>!condition</i> and only while <i>condition</i>.</li>
 * <li>first will drop the rest of the stream once condition is not met for the first time.</li>
 * <li>second will STOP dropping the elements and LEAVE the rest of the stream once condition is not met for the first time.</li>
 *
 * <h2> What's in it for me? </h2>
 * <li>drop what doesn't fit while streaming until an element fits, then leave the rest of the stream as it is - think {@link String#stripLeading()}</li>
 * <li>take only what fits while streaming until an element doesn't fit, then drop the rest of the stream - think {@link String#stripTrailing()}</li>
 * <li>new iterattion mechanics for streaming</li>
 *
 * @author Tomasz @LAFK_pl Borek
 * @since 9
 */
class StreamDoWhileOrUntil {

    public static void main(String[] args) {
        Predicate<Integer> isZero = i -> i == 0;
        Predicate<Integer> nonZero = i -> i != 0;
        final List<Integer> nums = List.of(0, 0, 3, 0, 0, 6, 0);
        System.out.println(nums);
        final List<Integer> takeIfNotAZeroUntilNonZeroHappensThenStopTaking =
                nums.stream().takeWhile(nonZero).collect(Collectors.toList());
        final List<Integer> takeZeroesUntilNonZeroHappensThenStopTaking =
                nums.stream().takeWhile(isZero).collect(Collectors.toList());
        System.out.format("Take != 0: %s %nTake == 0: %s %n", takeIfNotAZeroUntilNonZeroHappensThenStopTaking, takeZeroesUntilNonZeroHappensThenStopTaking);
        final List<Integer> dropIfNotAZeroUntilNonZeroHappensThenStopDropping =
                nums.stream().dropWhile(nonZero).collect(Collectors.toList());
        final List<Integer> dropZeroesUntilNonZeroHappensThenStopDropping =
                nums.stream().dropWhile(isZero).collect(Collectors.toList());
        System.out.format("Drop != 0: %s %nDrop == 0: %s %n", dropIfNotAZeroUntilNonZeroHappensThenStopDropping, dropZeroesUntilNonZeroHappensThenStopDropping);
    }
}
