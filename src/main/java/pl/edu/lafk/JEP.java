package pl.edu.lafk;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;

/**
 * JEP stands for Java Enhacement Proposal and usually describes a language feature to be added to Java (as in JDK, JRE, JVM, the language, the platform).
 * JEP 1 eplains what JEPS are about and is the default value for a JEP anotation.
 * @author Tomasz @LAFK_pl Borek
 * @see "JEP 1 - the JEP that talks about JEPs"
 */
@Documented
@Repeatable(value = JEPs.class)
public @interface JEP {
    String url() default "https://openjdk.java.net/jeps/1";
}
