package pl.edu.lafk.multithreading;

import pl.edu.lafk.JEP;

/**
 * Updates to process API
 *
 * @author Tomasz @LAFK_pl Borek
 * @since 9
 */
@JEP(url = "https://openjdk.java.net/jeps/102")
class ProcessUpdates {
    public static void main(String[] args) {
        ProcessHandle.current().destroyForcibly();
        System.out.println(ProcessHandle.current().info().user());
        ProcessHandle.allProcesses().filter(p -> p.info().totalCpuDuration().get().toHours() > 50).forEach(System.out::println);
    }
}
