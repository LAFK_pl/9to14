package pl.edu.lafk.multithreading;

/**
 * As part of this JEP, {@linkplain java.util.concurrent.CompletableFuture} class got several updates, mostly dealing with delays. Code changes covered most of the class.
 *
 * @author Tomasz @LAFK_pl Borek
 */
class ConcurrentUpdates2_CompletableFuture {
}
