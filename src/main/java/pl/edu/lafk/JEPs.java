package pl.edu.lafk;

import java.lang.annotation.Documented;

/**
 * When a bunch of JEPs were showcased in one class...
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Documented
public @interface JEPs {
    JEP[] value();
}
